<h3><img src="<?= $this->url->dir() ?>plugins/Msteams/msteams-icon.png"/>&nbsp;Mattermost</h3>
<div class="panel">
    <?= $this->form->label(t('Webhook URL'), 'msteams_webhook_url') ?>
    <?= $this->form->text('msteams_webhook_url', $values) ?>


    <p class="form-help"><a href="https://kanboard.net/plugin/msteams" target="_blank"><?= t('Help on MS Teams integration') ?></a></p>

    <div class="form-actions">
        <input type="submit" value="<?= t('Save') ?>" class="btn btn-blue"/>
    </div>
</div>
