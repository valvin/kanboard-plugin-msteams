<?php

namespace Kanboard\Plugin\Msteams;

use Kanboard\Core\Translator;
use Kanboard\Core\Plugin\Base;

/**
 * Microsoft Teams Plugin
 *
 * @package  msteams
 * @author   Valvin
 */
class Plugin extends Base
{
    public function initialize()
    {
        $this->template->hook->attach('template:config:integrations', 'msteams:config/integration');
        $this->template->hook->attach('template:project:integrations', 'msteams:project/integration');
        $this->projectNotificationTypeModel->setType('msteams', t('Msteams'), '\Kanboard\Plugin\Msteams\Notification\Msteams');
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getPluginDescription()
    {
        return 'Receive notifications on MS Teams';
    }

    public function getPluginAuthor()
    {
        return 'Valvin';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/valvin1/plugin-msteams';
    }

    public function getCompatibleVersion()
    {
        return '>=1.0.37';
    }
}
