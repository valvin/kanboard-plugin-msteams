Microsoft Teams plugin for Kanboard
===================================

Receive Kanboard notifications on Mattermost.

Author
------

- Frederic Guillot
- Valvin
- License MIT

Requirements
------------

- Kanboard >= 1.0.37
- Microsoft Teams (Office365)

Installation
------------

You have the choice between 3 methods:

1. Install the plugin from the Kanboard plugin manager in one click
2. Download the zip file and decompress everything under the directory `plugins/MsTeams`
3. Clone this repository into the folder `plugins/Msteams`

Note: Plugin folder is case-sensitive.

Configuration
-------------

Firstly, you have to generate a new webhook url in Microsoft Teams (**Connector > Incoming Webhooks**) in the channel you want receive notifications
Then in the project 
- in Integration paste you webhook URL. 
- in Notifications activate Msteams


## Troubleshooting

- Enable the debug mode
- All connection errors with the Mattermost API are recorded in the log files `data/debug.log`
